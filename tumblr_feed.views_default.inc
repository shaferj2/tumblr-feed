<?php

/**
 * Implements hook_views_default_views()
 */
function tumblr_feed_views_default_views() {
	$view = build_view();

	// add view to list of views
	$views[$view->name] = $view;

	return $views;
}

/** 
 * Saved exported view from within Drupal 
*/
function build_view() {
	$view = new view();
	$view->name = 'tumblr_blogs';
	$view->description = '';
	$view->tag = 'default';
	$view->base_table = 'node';
	$view->human_name = 'Tumblr Blogs';
	$view->core = 7;
	$view->api_version = '3.0';
	$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

	/* Display: Master */
	$handler = $view->new_display('default', 'Master', 'default');
	$handler->display->display_options['use_more_always'] = FALSE;
	$handler->display->display_options['access']['type'] = 'perm';
	$handler->display->display_options['cache']['type'] = 'none';
	$handler->display->display_options['query']['type'] = 'views_query';
	$handler->display->display_options['exposed_form']['type'] = 'basic';
	$handler->display->display_options['pager']['type'] = 'full';
	$handler->display->display_options['style_plugin'] = 'list';
	$handler->display->display_options['row_plugin'] = 'fields';
	/* Field: Content: Name */
	$handler->display->display_options['fields']['field_full_name']['id'] = 'field_full_name';
	$handler->display->display_options['fields']['field_full_name']['table'] = 'field_data_field_full_name';
	$handler->display->display_options['fields']['field_full_name']['field'] = 'field_full_name';
	$handler->display->display_options['fields']['field_full_name']['label'] = '';
	$handler->display->display_options['fields']['field_full_name']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['field_full_name']['element_default_classes'] = FALSE;
	$handler->display->display_options['fields']['field_full_name']['hide_alter_empty'] = FALSE;
	/* Field: Content: Image */
	$handler->display->display_options['fields']['field_image']['id'] = 'field_image';
	$handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
	$handler->display->display_options['fields']['field_image']['field'] = 'field_image';
	$handler->display->display_options['fields']['field_image']['label'] = '';
	$handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['field_image']['element_default_classes'] = FALSE;
	$handler->display->display_options['fields']['field_image']['hide_alter_empty'] = FALSE;
	$handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
	$handler->display->display_options['fields']['field_image']['settings'] = array(
	  'image_style' => '',
	  'image_link' => '',
	);
	/* Field: Content: Quote */
	$handler->display->display_options['fields']['field_quote']['id'] = 'field_quote';
	$handler->display->display_options['fields']['field_quote']['table'] = 'field_data_field_quote';
	$handler->display->display_options['fields']['field_quote']['field'] = 'field_quote';
	$handler->display->display_options['fields']['field_quote']['label'] = '';
	$handler->display->display_options['fields']['field_quote']['element_label_colon'] = FALSE;
	/* Field: Content: Blog URL */
	$handler->display->display_options['fields']['field_blog_url']['id'] = 'field_blog_url';
	$handler->display->display_options['fields']['field_blog_url']['table'] = 'field_data_field_blog_url';
	$handler->display->display_options['fields']['field_blog_url']['field'] = 'field_blog_url';
	$handler->display->display_options['fields']['field_blog_url']['label'] = '';
	$handler->display->display_options['fields']['field_blog_url']['exclude'] = TRUE;
	$handler->display->display_options['fields']['field_blog_url']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['field_blog_url']['element_default_classes'] = FALSE;
	$handler->display->display_options['fields']['field_blog_url']['hide_alter_empty'] = FALSE;
	$handler->display->display_options['fields']['field_blog_url']['click_sort_column'] = 'url';
	/* Field: Content: Post_1 */
	$handler->display->display_options['fields']['field_post_1']['id'] = 'field_post_1';
	$handler->display->display_options['fields']['field_post_1']['table'] = 'field_data_field_post_1';
	$handler->display->display_options['fields']['field_post_1']['field'] = 'field_post_1';
	$handler->display->display_options['fields']['field_post_1']['label'] = '';
	$handler->display->display_options['fields']['field_post_1']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['field_post_1']['element_default_classes'] = FALSE;
	$handler->display->display_options['fields']['field_post_1']['hide_alter_empty'] = FALSE;
	$handler->display->display_options['fields']['field_post_1']['click_sort_column'] = 'url';
	/* Field: Content: Post_2 */
	$handler->display->display_options['fields']['field_post_2']['id'] = 'field_post_2';
	$handler->display->display_options['fields']['field_post_2']['table'] = 'field_data_field_post_2';
	$handler->display->display_options['fields']['field_post_2']['field'] = 'field_post_2';
	$handler->display->display_options['fields']['field_post_2']['label'] = '';
	$handler->display->display_options['fields']['field_post_2']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['field_post_2']['element_default_classes'] = FALSE;
	$handler->display->display_options['fields']['field_post_2']['click_sort_column'] = 'url';
	/* Field: Content: Post_3 */
	$handler->display->display_options['fields']['field_post_3']['id'] = 'field_post_3';
	$handler->display->display_options['fields']['field_post_3']['table'] = 'field_data_field_post_3';
	$handler->display->display_options['fields']['field_post_3']['field'] = 'field_post_3';
	$handler->display->display_options['fields']['field_post_3']['label'] = '';
	$handler->display->display_options['fields']['field_post_3']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['field_post_3']['element_default_classes'] = FALSE;
	$handler->display->display_options['fields']['field_post_3']['click_sort_column'] = 'url';
	/* Sort criterion: Content: Post date */
	$handler->display->display_options['sorts']['created']['id'] = 'created';
	$handler->display->display_options['sorts']['created']['table'] = 'node';
	$handler->display->display_options['sorts']['created']['field'] = 'created';
	$handler->display->display_options['sorts']['created']['order'] = 'DESC';
	/* Filter criterion: Content: Published */
	$handler->display->display_options['filters']['status']['id'] = 'status';
	$handler->display->display_options['filters']['status']['table'] = 'node';
	$handler->display->display_options['filters']['status']['field'] = 'status';
	$handler->display->display_options['filters']['status']['value'] = 1;
	$handler->display->display_options['filters']['status']['group'] = 1;
	$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
	/* Filter criterion: Content: Type */
	$handler->display->display_options['filters']['type']['id'] = 'type';
	$handler->display->display_options['filters']['type']['table'] = 'node';
	$handler->display->display_options['filters']['type']['field'] = 'type';
	$handler->display->display_options['filters']['type']['value'] = array(
	  'tumblr_blog' => 'tumblr_blog',
	);

	return $view;
}