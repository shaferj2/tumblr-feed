<?php

/**
* Implements hook_form().
*/
function tumblr_feed_form() {
   $form = array();

   $form['some_text'] = array(
      '#markup' => '<h3>Go to the Tubmlr API developer console to get the keys!</h3>'
   );

   # Note: for variable_get, the second parameter is a default_value
   $form['tumblr_api_key'] = array(
      '#type' => 'textfield',
      '#title' => 'API key',
      '#description' => 'The Office of Admissions API key from Tumblr',
      '#default_value' => variable_get('tumblr_api_key'),
      '#size' => 100,
      '#maxlength' => 100,
      '#required' => TRUE,
   );

   $form['tumblr_secret_key'] = array(
      '#type' => 'textfield',
      '#title' => 'Secret key',
      '#description' => 'The Office of Admissions Secret key from Tumblr',
      '#default_value' => variable_get('tumblr_secret_key'),
      '#size' => 100,
      '#maxlength' => 100,
      '#required' => TRUE,
   );

   $form['tumblr_token'] = array(
      '#type' => 'textfield',
      '#title' => 'Token',
      '#description' => 'The Office of Admissions Token from Tumblr',
      '#default_value' => variable_get('tumblr_token'),
      '#size' => 100,
      '#maxlength' => 100,
      '#required' => TRUE,
   );

   $form['tumblr_token_secret'] = array(
      '#type' => 'textfield',
      '#title' => 'Token secret',
      '#description' => 'The Office of Admissions Token secret from Tumblr',
      '#default_value' => variable_get('tumblr_token_secret'),
      '#size' => 100,
      '#maxlength' => 100,
      '#required' => TRUE,
   );


   return system_settings_form($form);
}

/**
* Implements hook_form_validate().
*/
function tumblr_feed_form_validate($form, &$form_state) {
   if ($form == 'tumblr_feed_form') {
      $form_state['tumblr_api_key'] = check_plain($form_state['tumblr_api_key']);
      $form_state['tumblr_secret_key'] = check_plain($form_state['tumblr_secret_key']);
      $form_state['tumblr_token'] = check_plain($form_state['tumblr_token']);
      $form_state['tumblr_token_secret'] = check_plain($form_state['tumblr_token_secret']);
   }
}